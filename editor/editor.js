// AI HW3 Level Editor
// Written by Joel Brady (jbrady@cse.unsw.edu.au)
// 13s1 COMP3411

// constants
var NUM_COLS = 80;
var NUM_ROWS = 80;
var GRID_LINE_WIDTH = 1;
var WATER = "#0000FF";
var GROUND = "#FFFFFF";
var DOOR = "#FF0000";
var AXE = "#00CCFF"
var TREE = "#00CC00";
var DYNAMITE = "#CC33FF";
var GOLD = "#FFFF00";
var WALL = "#000000";

var solid_colors = ["water", "ground", "door", "axe", "tree", "dynamite", "gold", "wall"];
var color_lookup = [WATER, GROUND, DOOR, AXE, TREE, DYNAMITE, GOLD, WALL];

// globals
var context;
var debug_text_area;
var canvas;
var vertical_cell_size;
var horizontal_cell_size;
var mouse_down = false;
var current_tile;
var grid = new Array();

$(window).load(main);

function log(message) {
    $('#debug_log').scrollTop($('#debug_log')[0].scrollHeight);
    debug_text_area.value += "\n" + message;
}

function init() {
    canvas = $("#editor_canvas")[0]
    context = canvas.getContext("2d");
    context.fillStyle = WATER;
    debug_text_area = $("#debug_log")[0];
    vertical_cell_size = (canvas.height - GRID_LINE_WIDTH * NUM_ROWS - 1) / NUM_ROWS;
    horizontal_cell_size = (canvas.width - GRID_LINE_WIDTH * NUM_COLS - 1) / NUM_COLS;

    for (var row = 0; row < NUM_ROWS; ++row) {
        grid[row] = new Array();
        for (var col = 0; col < NUM_COLS; ++col) {
            grid[row][col] = "ground";
        }
    }

    $("#editor_canvas").mousedown(function (e) {
        mouse_down = true;
        draw(e);
    });
    $("#editor_canvas").mouseup(function () {
            mouse_down = false;
    });
    $("#editor_canvas").mousemove(function (e) {
        draw(e);
    });
    $("#fill_button").click(fill_entire_grid);

    $("#water_button").click(function () { set_current_tile("water"); });
    $("#door_button").click(function () { set_current_tile("door"); });
    $("#tree_button").click(function () { set_current_tile("tree"); });
    $("#wall_button").click(function () { set_current_tile("wall"); });
    $("#ground_button").click(function () { set_current_tile("ground"); });
    $("#axe_button").click(function () { set_current_tile("axe"); });
    $("#dynamite_button").click(function () { set_current_tile("dynamite"); });
    $("#key_button").click(function () { set_current_tile("key"); });
    $("#gold_button").click(function () { set_current_tile("gold"); });

    $("#dump_button").click(dump_level);
    $("#load_button").click(load_level);

    set_current_tile("water");
}

function set_status_text(text) {
    $("#status_text")[0].value = text;
}

function set_current_tile(tile) {
    current_tile = tile;
    set_status_text(tile);
}

function main() {
    init();
    draw_grid();
}

function draw_grid() {
    var oldStyle = context.fillStyle;
    context.fillStyle="#000000";

    // horizontal lines
    for (var i = 1; i < NUM_ROWS; ++i) {
        context.fillRect(0, i * (vertical_cell_size + GRID_LINE_WIDTH), canvas.width, GRID_LINE_WIDTH);
    }

    // vertical lines
    for (var j = 1; j < NUM_COLS; ++j) {
        context.fillRect(j * (horizontal_cell_size + GRID_LINE_WIDTH), 0, GRID_LINE_WIDTH, canvas.height);
    }
    context.fillStyle = oldStyle;
}

function fill_cell(x, y) {
    var index = $.inArray(current_tile, solid_colors);
    if (index > -1) {
        context.fillStyle = color_lookup[index];
        context.fillRect(x * (horizontal_cell_size + GRID_LINE_WIDTH)+1, y * (vertical_cell_size + GRID_LINE_WIDTH)+1, horizontal_cell_size, vertical_cell_size);
    } else {
        draw_image(x, y);
    }
    grid[x][y] = current_tile;
}

function draw_image(x, y) {
        var img = $("#" + current_tile)[0];
        context.drawImage(img, x * (horizontal_cell_size + GRID_LINE_WIDTH), y * (vertical_cell_size + GRID_LINE_WIDTH), horizontal_cell_size, vertical_cell_size);
}

function draw(e) {
    if (mouse_down === true) {
        var pos = pos_to_coord(e);
        fill_cell(pos.x, pos.y);
    }
}

function pos_to_coord(e) {
    // e.offset(X|Y)
    var ret = new Object();
    ret.x = Math.floor(e.offsetX / (horizontal_cell_size + GRID_LINE_WIDTH));
    ret.y = Math.floor(e.offsetY / (vertical_cell_size + GRID_LINE_WIDTH));
    return ret;
}

function fill_entire_grid() {
    for (var x = 0; x < NUM_COLS; ++x) {
        for (var y = 0; y < NUM_ROWS; ++y) {
            fill_cell(x, y);
        }
    }
}

function dump_level() {
    var level = "";
    for (var col = 0; col < NUM_COLS; ++col) {
        for (var row = 0; row < NUM_ROWS; ++row) {
            var tile = grid[row][col];
            var char;
            switch(tile) {
                case "water":
                    char = "~"
                break;
                case "ground":
                    char = " ";
                break;
                case "tree":
                    char = "T";
                break;
                case "axe":
                    char = "a";
                break;
                case "key":
                    char = "k";
                break;
                case "door":
                    char = "-";
                break;
                case "wall":
                    char = "*";
                break;
                case "dynamite":
                    char = "d";
                break;
                case "gold":
                    char = "g";
                break;
                default:
                    log("somethign broke, recommend restarting page");
                break;
            }
            level += char;
        }
        level += "\n";
    }
    $("#io").text(level);
}

function load_level() {
    var value = $("#io").val();
    var level = value.replace("\n", "penis");
    for (var row = 0; row < NUM_ROWS; ++row) {
        for (var col = 0; col < NUM_COLS; ++col) {
            var tile = level[row*NUM_COLS+col];
            var entry;
            switch(tile) {
                case "~":
                    entry = "water"
                break;
                case " ":
                    entry = "ground";
                break;
                case "T":
                    entry = "tree";
                break;
                case "a":
                    entry = "axe";
                break;
                case "k":
                    entry = "key";
                break;
                case "-":
                    entry = "door";
                break;
                case "*":
                    entry = "wall";
                break;
                case "d":
                    entry = "dynamite";
                break;
                case "g":
                    entry = "gold";
                break;
                default:
                    log("somethign broke, recommend restarting page");
                break;
            }
            grid[row][col] = entry;
            set_current_tile(entry);
            fill_cell(row, col);
        }
    }
}