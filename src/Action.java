/**
 * joel brady jbrady@cse.unsw.edu.au
 */
public enum Action {
    LEFT,
    RIGHT,
    FORWARD,
    CHOP,
    OPEN,
    BLAST;

    public char toChar() {
        switch(this) {
            case LEFT:
                return 'L';
            case RIGHT:
                return 'R';
            case FORWARD:
                return 'F';
            case CHOP:
                return 'C';
            case OPEN:
                return 'O';
            case BLAST:
                return 'B';
        }
        // will never reach here
        return 'Z';
    }


}
