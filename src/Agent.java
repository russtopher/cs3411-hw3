/**
 * joel brady jbrady@cse.unsw.edu.au
 * comp3411 hw3 13s1
 *
 * TODO block of comments answering question here
 */
public class Agent {

    private static Communicator communicator;

    public static void main(String[] args) {
        // this should be run as java Agent -p <portno>
        setup(args);
        Thinker thinker = new Thinker();

        while(!thinker.isFinished()) {
            // read state from socket
            byte buf[] = communicator.getData();

            // give to agent
            Window win = new Window(buf);
            thinker.giveState(win);

            char move = thinker.getMove();

            // send to socket
            communicator.sendMove(move);
        }
        communicator.end();
    }

    private static void setup(String[] args) {
        if (args.length != 2) {
            System.err.println("Invalid number of arguments");
            System.err.println("Run as:");
            System.err.println("\tjava Agent -p <portnum>");
            System.exit(1);
        }

        int port = -1;
        try {
            port = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            System.err.println("Invalid port number " + args[1]);
            System.exit(1);
        }
        communicator = new Communicator(port);
    }
}
