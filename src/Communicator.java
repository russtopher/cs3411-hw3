import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * joel brady jbrady@cse.unsw.edu.au
 * comp3411 hw3 13s1
 */
public class Communicator {

    private Socket socket;
    private InputStream input;
    private OutputStream output;

    public Communicator(int port) {
        try {
            socket = new Socket("127.0.0.1", port);
            input = socket.getInputStream();
            output = socket.getOutputStream();
        } catch (IOException e) {
            System.err.println("Error opening port " + port);
            System.exit(1);
        }
    }

    public byte[] getData() {
        byte[] buf = new byte[24];
        try {
            int bytesRead = input.read(buf);
            while (bytesRead != buf.length) {
                bytesRead += input.read(buf, bytesRead, buf.length - bytesRead);
            }
        } catch (IOException e) {
            System.err.println("error reading socket");
            System.exit(1);
        }
        return buf;
    }

    public void sendMove(char move) {
        try {
            output.write(move);
        } catch (IOException e) {
            System.err.println("error writing to socket");
            System.exit(1);
        }
    }

    public void end() {

        try {
            input.close();
            output.close();
            socket.close();
        } catch (IOException e) {
            System.err.println("error closing socket");
            System.exit(1);
        }
    }
}
