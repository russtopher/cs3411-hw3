import java.util.ArrayList;
import java.util.List;

/**
 * joel brady jbrady@cse.unsw.edu.au
 * comp3411 hw3 13s1
 */
public enum Direction {
    NORTH,
    EAST,
    WEST,
    SOUTH;

    public static final Direction[] values = values();

    private enum Turn {
        LEFT,
        RIGHT
    }

    private Direction rotate(Turn t) {
        switch(this) {
            case NORTH:
                if (t == Turn.LEFT) {
                    return WEST;
                } else {
                    return EAST;
                }
            case SOUTH:
                if (t == Turn.LEFT) {
                    return EAST;
                } else {
                    return WEST;
                }
            case WEST:
                if (t == Turn.LEFT) {
                    return SOUTH;
                } else {
                    return NORTH;
                }
            case EAST:
                if (t == Turn.LEFT) {
                    return NORTH;
                } else {
                    return SOUTH;
                }
            default:
                assert false;
                return null;
        }
    }

    public Direction turnLeft() {
        return rotate(Turn.LEFT);
    }

    public Direction turnRight() {
        return rotate(Turn.RIGHT);
    }

    public List<Character> getMovesForTurnTo(Direction to) {
        List<Character> list = new ArrayList<Character>();

        if (this == to) {
            return list;
        } else if (this.turnLeft() == to) {
            list.add('L');
        } else if (this.turnRight() == to) {
            list.add('R');
        } else {
            list.add('L');
            list.add('L');
        }

        return list;
    }

    @Override
    public String toString() {
        switch(this) {
            case NORTH:
                return "^";
            case SOUTH:
                return "v";
            case WEST:
                return "<";
            case EAST:
                return ">";
            default:
                assert false;
                return null;
        }
    }
}