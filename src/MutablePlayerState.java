/**
 * joel brady jbrady@cse.unsw.edu.au
 */
public class MutablePlayerState implements PlayerState {

    private boolean gold = false;
    private boolean key = false;
    private boolean axe = false;
    private int dynamite = 0;

    public MutablePlayerState(MutablePlayerState state) {
        this.dynamite = state.dynamite;
        this.axe = state.axe;
        this.key = state.key;
        this.gold = state.gold;
    }

    public MutablePlayerState() {

    }

    @Override
    public boolean hasGold() {
        return gold;
    }

    @Override
    public boolean hasKey() {
        return key;
    }

    @Override
    public boolean hasAxe() {
        return axe;
    }

    @Override
    public boolean hasDynamite() {
        return dynamite > 0;
    }

    public void giveGold() {
        gold = true;
    }

    public void giveAxe() {
        axe = true;
    }

    public void giveDynamite() {
        dynamite++;
    }

    public void giveKey() {
        key = true;
    }

    public void useDynamite() {
        assert dynamite > 0;
        dynamite--;
    }
}
