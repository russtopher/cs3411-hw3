import java.util.*;

/**
 * joel brady jbrady@cse.unsw.edu.au
 */
public class PathFinder {

    private WorldModel world;

    public PathFinder(WorldModel world) {
        this.world = world;
    }

    public void findPositionWhereWeCanSeeUnvisited(Queue<Position> path) {
        Queue<State> q = new ArrayDeque<State>();
        Set<Position> seen = new HashSet<Position>();
        q.add(new State(world.currentPosition()));

        State current;
        do {
            current = q.remove();
            for (Direction dir : Direction.values) {
                Position newPos = current.position.move(dir);
                if (world.isOccupiable(newPos) || world.canSeeUnvisited(current.position)) {
                    if (!seen.contains(newPos)) {
                        q.add(new State(newPos, current));
                        seen.add(newPos);
                    }
                }
            }
        } while (!world.canSeeUnvisited(current.position) && q.size() > 0);

        if (world.canSeeUnvisited(current.position)) {
            buildPathFromState(current, path);
            path.remove();
        }
    }

    public void findNearestUnvisited(Queue<Position> path) {
        Queue<State> q = new ArrayDeque<State>();
        Set<Position> seen = new HashSet<Position>();
        q.add(new State(world.currentPosition()));

        State current;
        do {
            current = q.remove();
            for (Direction dir : Direction.values) {
                Position newPos = current.position.move(dir);
                if (world.isOccupiable(newPos)
                        || world.atPosition(current.position.move(dir)) == Tile.UNVISITED) {
                    if (!seen.contains(newPos)) {
                        q.add(new State(newPos, current));
                        seen.add(newPos);
                    }
                }
            }
        } while (world.atPosition(current.position) != Tile.UNVISITED && q.size() > 0);

        if (world.atPosition(current.position) == Tile.UNVISITED) {
            buildPathFromState(current, path);
            path.remove();
        }
    }

    public Queue<Position> generatePath(Position src, Position dst) {
        Queue<State> q = new PriorityQueue<State>();
        Set<Position> seen = new HashSet<Position>();

        // DO THE SEARCH BACKWARDS, LIKE A BOSS
        q.add(new State(dst, src, new WorldModel(world)));
        seen.add(q.peek().position);

        State current;
        do {
            current = q.remove();
            Position curPos = current.position;
            for (Direction dir : Direction.values) {
                Position newPos = curPos.move(dir);
                if (current.world.isOccupiable(newPos) || dst.equals(newPos)) {
                    if (!seen.contains(newPos)) {
                    State s = new State(newPos, current);
                        s.world.moveTo(newPos);
                        q.add(s);
                        seen.add(newPos);
                    }
                }
            }
        } while (!current.atGoal() && q.size() > 0);

        ArrayDeque<Position> result = new ArrayDeque<Position>();
        if (current.atGoal()) {
            buildPathFromState(current, result);

            // reverse the queue since we did the search backwards
            // TODO have a method build it the other way
            ArrayDeque<Position> tmp = result;
            result = new ArrayDeque<Position>();
            for (Position p : tmp) {
                result.addFirst(p);
            }
            result.remove(); // remove the node we're already at
        }
        return result;
    }

    private void buildPathFromState(State s, Queue<Position> path) {
        if (s != null) {
            buildPathFromState(s.parent, path);
            path.add(s.position);
        }
    }



}
