/**
 * joel brady jbrady@cse.unsw.edu.au
 */
public interface PlayerState {

    public boolean hasGold();

    public boolean hasKey();

    public boolean hasAxe();

    public boolean hasDynamite();
}
