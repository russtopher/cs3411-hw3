/**
 * joel brady jbrady@cse.unsw.edu.au
 * Immutable Position class
 */
public class Position {
    public final int row;
    public final int col;

    public Position(int row, int col) {
        this.row = row;
        this.col = col;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        if (col != position.col) return false;
        if (row != position.row) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = row;
        result = 31 * result + col;
        return result;
    }

    public Position move(Direction d) {
        switch (d) {
            case NORTH:
                return new Position(row - 1, col);
            case SOUTH:
                return new Position(row + 1, col);
            case WEST:
                return new Position(row, col - 1);
            case EAST:
                return new Position(row, col + 1);
            default:
                assert false;
                return null;
        }
    }

    @Override
    public String toString() {
        return row + " " + col;
    }
}
