/**
 * joel brady jbrady@cse.unsw.edu.au
 */
public class State implements Comparable<State>{

    public final Position position;
    public State parent;
    public Position goal;
    public int distance;
    public final WorldModel world;

    /**
     * Create a state starting at the given world's current position
     * @param world
     */
    public State(WorldModel world) {
        this(world.currentPosition(), world);
    }

    public State(Position p) {
        this.position = p;
        this.world = null;
    }

    public State(Position p, WorldModel world) {
        this.position = p;
        this.parent = null;
        this.goal = null;
        this.distance = 0;
        this.world = world;
    }

    public State(Position p, Position goal, WorldModel world) {
        this(p, world);
        this.goal = goal;
    }

    public State(Position p, State parent) {
        this(p, parent.world != null ? new WorldModel(parent.world) : null);
        this.parent = parent;
        this.distance = this.parent.distance + 1;
        this.goal = this.parent.goal;
    }

    public boolean atGoal() {
        return this.position.equals(goal);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        State state = (State) o;

        if (!position.equals(state.position)) return false;

        return true;
    }

    private int manhattan() {
        return Math.abs(goal.row - this.position.row) + Math.abs(goal.row - this.position.row);
    }

    @Override
    public int compareTo(State o) {
        return  (this.manhattan() + this.distance) - (o.manhattan() + o.distance);
    }

    @Override
    public int hashCode() {
        return position.hashCode();
    }

    @Override
    public String toString() {
        String p = "none";
        if (parent != null) {
            p = parent.position.toString();
        }
        return position + " parent: " + p;
    }
}
