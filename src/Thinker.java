import java.util.ArrayDeque;
import java.util.List;
import java.util.Queue;

/**
 * joel brady jbrady@cse.unsw.edu.au
 * comp3411 hw3 13s1
 */
public class Thinker {

    private final WorldModel world;
    private final PathFinder pathFinder;

    private Queue<Position> path;
    private final Queue<Character> moves;

    private boolean finished = false;

    public Thinker() {

        moves = new ArrayDeque<Character>();
        world = new WorldModel();
        pathFinder = new PathFinder(world);
        path = new ArrayDeque<Position>();
    }

    public char getMove() {
        // if we already have moves decided
        // then we don't need to do any thinking
        if (moves.size() > 0) {
            // go straight to doing a move at the end
        } else if (path.size() > 0) {
            goToPosition(path.remove());
            if (moves.size() == 0) {
                // throw out the path, something fucked up
                path.clear();
            }

        } else if (world.getPlayerState().hasGold()) {
            // go back to start
            path = pathFinder.generatePath(world.currentPosition(), WorldModel.START_POSITION);
            goToPosition(path.remove());

        // TODO maybe simplify this to include gold
        } else if (world.availableItem()) {
            List<Position> items = world.getItemPositions();

            for (Position item : items) {
                path = pathFinder.generatePath(world.currentPosition(), item);
                if (path.size() > 0) {
                    System.out.print("found item path at " + item);
                    System.out.println(" item is " + world.atPosition(item));
                    break;
                }
            }

        } else if (world.getGoldPosition() != null) {
            path = pathFinder.generatePath(world.currentPosition(), world.getGoldPosition());

            if (path.size() > 0) {
                Position target = path.remove();
                goToPosition(target);
            }
        }

        if (moves.size() == 0) {
            if (path.size() == 0) {
                pathFinder.findNearestUnvisited(path);
            }
            if (path.size() == 0) {
                pathFinder.findPositionWhereWeCanSeeUnvisited(path);
            }

            // we are assuming that if we are here, then we haven't found
            // a solution, hence there must be more to visit
            assert path.size() > 0;
            goToPosition(path.remove());
        }

        assert moves.size() > 0;
        Character move = moves.remove();
        world.doMove(move);
        if (world.getPlayerState().hasGold() && world.atStart()) {
            finished = true;
        }
        return move;
    }

    private void goToPosition(Position target) {
        Direction directionToTarget = positionToDirection(target);
        Direction worldDirection = world.getDirection();
        if (directionToTarget != worldDirection) {
            moves.addAll(worldDirection.getMovesForTurnTo(directionToTarget));
        }
        switch (world.atPosition(target)) {
            case WALL:
                // TODO dynamite can be used for other things too
                if (world.getGoldPosition() != null && world.getPlayerState().hasDynamite()) {
                    moves.add('B');
                } else {
                    abort();
                    return;
                }
                break;
            case WATER:
                // abort abort!
                abort();
                return;
            case DOOR:
                if (world.getPlayerState().hasKey()) {
                    moves.add('O');
                } else {
                    abort();
                    return;
                }
                break;
            case TREE:
                if (world.getPlayerState().hasAxe()) {
                    moves.add('C');
                } else {
                    abort();
                    return;
                }
                break;
        }
        moves.add('F');
    }

    private void abort() {
        while (path.size() > 1) {
            path.remove();
        }
        moves.clear();
        path.clear();
    }

    private Direction positionToDirection(Position target) {
        return positionToDirection(target, world.currentPosition());
    }

    private static Direction positionToDirection(Position target, Position current) {
        if (target.row == current.row + 1) {
            // we can only move up/down/left/right
            assert target.col == current.col;
            return Direction.SOUTH;
        } else if (target.row == current.row - 1) {
            assert target.col == current.col;
            return Direction.NORTH;
        } else if (target.col == current.col + 1) {
            assert target.row == current.row;
            return Direction.EAST;
        } else {
            assert target.col == current.col - 1;
            assert target.row == current.row;
            return Direction.WEST;
        }
    }

    public void giveState(Window win) {
        world.giveState(win);
    }

    public boolean isFinished() {
        return finished;
    }
}
