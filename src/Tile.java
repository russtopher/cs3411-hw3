/**
 * joel brady jbrady@cse.unsw.edu.au
 * comp3411 hw3 13s1
 */

public enum Tile {
    GROUND,
    WATER,
    DOOR,
    KEY,
    AXE,
    TREE,
    GOLD,
    DYNAMITE,
    WALL,
    UNVISITED;

    @Override
    public String toString() {
        switch(this) {
            case GROUND:
                return " ";
            case WATER:
                return "~";
            case DOOR:
                return "-";
            case KEY:
                return "k";
            case DYNAMITE:
                return "d";
            case GOLD:
                return "g";
            case AXE:
                return "a";
            case TREE:
                return "T";
            case WALL:
                return "*";
            case UNVISITED:
                return ".";
        }
        assert false;
        return null;
    }
    
    public static Tile getTile(byte b) {
        switch (b) {
            case ' ':
                return GROUND;
            case '~':
                return WATER;
            case '-':
                return DOOR;
            case 'k':
                return KEY;
            case 'd':
                return DYNAMITE;
            case 'g':
                return GOLD;
            case 'a':
                return AXE;
            case 'T':
                return TREE;
            case '*':
                return WALL;
            case '.':
                return UNVISITED;
        }
        return null;
    }
}

