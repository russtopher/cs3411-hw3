/**
 * joel brady jbrady@cse.unsw.edu.au
 */
public class Window {

    public static final int WINDOW_ROWS = 5;
    public static final int WINDOW_COLS = 5;

    public final Tile[][] tiles;

    public Window(Tile[][] tiles) {
        this.tiles = new Tile[WINDOW_ROWS][WINDOW_COLS];
        for (int row = 0; row < WINDOW_ROWS; ++row) {
            System.arraycopy(tiles[row], 0, this.tiles[row], 0, WINDOW_COLS);
        }
    }

    public Window (byte[] buf) {
        this.tiles = new Tile[WINDOW_ROWS][WINDOW_COLS];

        int i = 0;
        for (int row = 0; row < WINDOW_ROWS; ++row) {
            for (int col = 0; col < WINDOW_COLS; ++col) {
                if (!(row == 2 && col == 2)) {
                    tiles[row][col] = Tile.getTile(buf[i]);
                    ++i;
                } else {
                    // player's position
                    tiles[row][col] = null;
                }
            }
        }
    }

    public void print() {
        System.out.println("+-----+");
        for (Tile[] row : tiles) {
            System.out.print("|");
            for (Tile t : row) {
                if (t != null) {
                    System.out.print(t.toString());
                } else {
                    System.out.print("^");
                }
            }
            System.out.println("|");
        }
        System.out.println("+-----+\n\n");
    }

    public Window rotate(Direction d) {
        Tile[][] result = new Tile[WINDOW_ROWS][WINDOW_COLS];
        int orow;
        int ocol;
        switch (d) {
            case SOUTH:
                orow = 0;
                ocol = 0;
                for (int row = WINDOW_ROWS - 1; row >= 0; --row) {
                    for (int col = WINDOW_COLS - 1; col >= 0; --col) {
                        result[row][col] = tiles[orow][ocol];
                        ocol++;
                    }
                    ocol = 0;
                    orow++;
                }
                break;

            case WEST:
                orow = 0;
                ocol = 4;
                for (int row = 0; row < WINDOW_ROWS; ++row) {
                    for (int col = 0; col < WINDOW_COLS; ++col) {
                        result[row][col] = tiles[orow][ocol];
                        orow++;
                    }
                    orow = 0;
                    ocol--;
                }
                break;

            case EAST:
                orow = 4;
                ocol = 0;
                for (int row = 0; row < WINDOW_ROWS; ++row) {
                    for (int col = 0; col < WINDOW_COLS; ++col) {
                        result[row][col] = tiles[orow][ocol];
                        orow--;
                    }
                    orow = 4;
                    ocol++;
                }
                break;

            case NORTH:
                // don't rotate, give back original
                return new Window(this.tiles);
        }
        return new Window(result);
    }
}
