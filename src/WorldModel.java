import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * joel brady jbrady@cse.unsw.edu.au
 */
public class WorldModel {

    private final Tile[][] grid;
    private static final int NUM_ROWS = 200;
    private static final int NUM_COLS = 200;

    public static final Position START_POSITION = new Position(100, 100);

    private Direction direction;
    private Position position;
    private final MutablePlayerState playerState;

    private Position gold;
    private final List<Position> axe = new ArrayList<Position>();
    private final List<Position> dynamite = new ArrayList<Position>();
    private final List<Position> key = new ArrayList<Position>();
    private final List<List<Position>> itemLists = new ArrayList<List<Position>>();

    private final Map<Position, Boolean> memo_table = new HashMap<Position, Boolean>();

    public WorldModel() {
        grid = new Tile[NUM_ROWS][NUM_COLS];
        for (int row = 0; row < NUM_ROWS; ++row) {
            for (int col = 0; col < NUM_COLS; ++col) {
                grid[row][col] = Tile.UNVISITED;
            }
        }

        setPosition(START_POSITION, Tile.GROUND);
        direction = Direction.NORTH;
        position = START_POSITION;
        itemLists.add(axe);
        itemLists.add(dynamite);
        itemLists.add(key);
        playerState = new MutablePlayerState();
    }

    /**
     * Copy constructor
     * @param world the world we are making a copy of
     */
    public WorldModel(WorldModel world) {
        // straight up copy of immutable fields
        direction = world.direction;
        position = world.position;
        gold = world.gold;

        playerState = new MutablePlayerState(world.playerState);

        grid = new Tile[NUM_ROWS][NUM_COLS];
        for (int row = 0; row < NUM_ROWS; ++row) {
            System.arraycopy(world.grid[row], 0, grid[row], 0, NUM_COLS);
        }

        axe.addAll(world.axe);
        dynamite.addAll(world.dynamite);
        key.addAll(world.key);
        itemLists.add(axe);
        itemLists.add(dynamite);
        itemLists.add(key);
        memo_table.putAll(world.memo_table);
    }

    public void giveState(Window w) {
        Window rotatedWindow = w.rotate(direction);

        // tiles[0][0] is player pos.x - 2, pos.y - 2
        for (int row = 0; row < Window.WINDOW_COLS; ++row) {
            for (int col = 0; col < Window.WINDOW_ROWS; ++col) {
                int gridRow = position.row - 2 + row;
                int gridCol = position.col - 2 + col;
                if (row == 2 && col == 2) {
                    continue;
                }
                if (grid[gridRow][gridCol] == Tile.UNVISITED) {
                    assert rotatedWindow.tiles[row][col] != null;
                    grid[gridRow][gridCol] = rotatedWindow.tiles[row][col];

                    // detect if we see an item
                    detectItem(grid[gridRow][gridCol], gridRow, gridCol);
                } else {
                    assert grid[gridRow][gridCol]
                                == rotatedWindow.tiles[row][col];
                }
            }
        }
    }

    private void detectItem(Tile t, int gridRow, int gridCol) {
        switch (t) {
            case GOLD:
                gold = new Position(gridRow, gridCol);
                System.out.println("found gold at " + gold);
                break;
            case AXE:
                axe.add(new Position(gridRow, gridCol));
                break;
            case DYNAMITE:
                dynamite.add(new Position(gridRow, gridCol));
                break;
            case KEY:
                key.add(new Position(gridRow, gridCol));
                break;
        }
    }

    public void printAroundAvatar() {
        System.out.println("+--------------------+");
        for (int row = 0; row < 20; ++row) {
            System.out.print("|");
            for (int col = 0; col < 20; ++col) {
                System.out.print(grid[position.row - 10 + row][position.col - 10 + col]);
            }
            System.out.println("|");
        }
        System.out.println("+--------------------+");
    }

    public Position currentPosition() {
        return position;
    }

    public boolean atStart() {
        return position.equals(START_POSITION);
    }

    public Direction getDirection() {
        return direction;
    }

    private void setPosition(Position p, Tile t) {
        grid[p.row][p.col] = t;
    }

    public Tile atPosition(Position p) {
        return grid[p.row][p.col];
    }

    public Position getPosition(Tile t) {
        for (int row = 0; row < NUM_ROWS; ++row) {
            for (int col = 0; col < NUM_COLS; ++col) {
                if (grid[row][col] == t) {
                    return new Position(row, col);
                }
            }
        }
        return null;
    }

    public boolean isOccupiable(Position p) {
        boolean result = true;
        switch (grid[p.row][p.col]) {
            case DOOR:
                if (playerState.hasKey()) {
                    break;
                }
                result = false;
                break;
            case TREE:
                if (playerState.hasAxe()) {
                    break;
                }
                result = false;
                break;
            case WALL:
                // only allow blowing up a wall if we've seen the gold
                if (playerState.hasDynamite() && gold != null) {
                    break;
                }
                result = false;
                break;
            case UNVISITED:
            case WATER:
                result = false;
                break;
            default:
                break;
        }
        return result;
    }

    private void turnLeft() {
        direction = direction.turnLeft();
    }

    private void turnRight() {
        direction = direction.turnRight();
    }

    private void goForward() {
        position = position.move(direction);
        moveTo(position);
    }

    /**
     * Update world state so we go to pos,
     * if we use an item, make sure it is consumed
     * @param pos where we're going
     */
    public void moveTo(Position pos) {
        switch (atPosition(pos)) {
            case KEY:
                playerState.giveKey();
                key.remove(position);
                break;
            case DYNAMITE:
                playerState.giveDynamite();
                dynamite.remove(position);
                break;
            case AXE:
                playerState.giveAxe();
                axe.remove(position);
                break;
            case GOLD:
                playerState.giveGold();
                break;
            case WALL:
                playerState.useDynamite();
                break;

        }
        setPosition(position, Tile.GROUND);
        position = pos;

        // now that we've visited position, the 25 surrounding
        // tiles are all visited now, update memo_table
        memo_table.put(position, false);
    }

    public void doMove(Character move) {
        switch(move) {
            case 'F':
                goForward();
                break;
            case 'L':
                turnLeft();
                break;
            case 'R':
                turnRight();
                break;
            case 'B':
                playerState.useDynamite();
            case 'O':
            case 'C':
                Position tile = position.move(direction);
                setPosition(tile, Tile.GROUND);
                break;
        }
    }

    public PlayerState getPlayerState() {
        return playerState;
    }

    public Position getGoldPosition() {
        return gold;
    }

    public boolean availableItem() {
        return axe.size() > 0 || dynamite.size() > 0 || key.size() > 0;
    }

    public List<Position> getItemPositions() {
        List<Position> items = new ArrayList<Position>();
        for (List<Position> list : itemLists) {
            for (Position item : list) {
                items.add(item);
            }
        }
        return items;
    }

    /**
     * Checks if there is a tile of type tile within
     * a windows distance of pos
     * @param tile the type of tile we're looking for
     * @param pos the centre of the window
     */
    private boolean withinWindow(Tile tile, Position pos) {
        for (int row = -2 + pos.row; row <= 2 + pos.row; ++row) {
            for (int col = -2 + pos.col; col <= 2 + pos.col; ++col) {
                if (grid[row][col] == tile) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean canSeeUnvisited(Position p) {
        if (memo_table.containsKey(p)) {
            return memo_table.get(p);
        } else {
            boolean answer = withinWindow(Tile.UNVISITED, p);
            memo_table.put(p, answer);
            return answer;
        }

    }
}
